require 'rails_helper'

RSpec.describe Question, type: :model do

  context 'VALIDATIONS' do

    it 'is invalid without a user' do

      expect(
          build :question, user: nil
      ).to_not be_valid

    end

    it 'is invalid without a statement' do

      expect(
          build :question, statement: nil
      ).to_not be_valid

    end

    it 'is invalid if statement exceeds 300 characters' do

      expect(
          build :question, statement: Faker::Lorem.sentence(301)
      ).to_not be_valid

      sentence = ''
      299.times{ sentence << 'a' }
      sentence << ' '
      expect(
          build :question, statement: sentence
      ).to be_valid

    end

    it 'is invalid if statement is less than 20 characters' do

      sentence = ''
      19.times{ sentence << 'a' }
      sentence << ' '
      expect(
          build :question, statement: sentence
      ).to be_valid

      sentence = ''
      18.times{ sentence << 'a' }
      sentence << ' '
      expect(
          build :question, statement: sentence
      ).to_not be_valid

    end

    it 'is invalid if statement does not contain at least 1 space in it' do
      sentence = 'jskckjsdkjcksjdcjkskjdckjsdcjkskdcnkjsdnc'
      expect(
          build :question, statement: sentence
      ).to_not be_valid
    end

    it 'is invalid without slug' do

      question = create :question

      question.slug = nil

      expect(question).to_not be_valid

    end

  end

  context 'Associations' do

    it 'is destroyed as the associated use is destroyed' do

      user = create :user

      2.times{create :question, user: user}

      expect{
        user.destroy
      }.to change(Question, :count).by(-2)


    end

  end

end