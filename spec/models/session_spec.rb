require 'rails_helper'

RSpec.describe Session, type: :model do

  context 'Validations' do

    it 'is invalid without a user id' do
      expect(
          build(:session, user: nil)
      ).to_not be_valid
    end

    it 'is invalid without an ip_address' do
      expect(
          build :session, ip_address: nil
      ).to_not be_valid
    end

    it 'is invalid without an origin' do
      expect(
          build :session, origin: nil
      ).to_not be_valid
    end

    #there is only one active session from 1 ip_address -- WRONG

    #SESIONS WITH DUPLICATE ORIGIN OR IP_ADDRESS ARE VALID AS LONG AS USER_ID IS DIFFERENT

    it 'is invalid if all of user_id, ip_address and origin are duplicate' do
      session = create :session
      expect(
          build :session, user: session.user, origin: session.origin, ip_address: session.ip_address
      ).to_not be_valid
    end

    # =======WRONG====
    # it 'is invalid for duplicate ip_address' do
    #   session = create :session
    #   expect(
    #       build :session, ip_address: session.ip_address
    #   ).to_not be_valid
    # end
    #
    # #each valid origin should have at-most one active session
    # it 'is invalid for duplicate origin' do
    #   session = create :session
    #   expect(
    #       build :session, origin: session.origin
    #   ).to_not be_valid
    # end
    #=====WRONG=======

  end

  describe 'Session.fetch_token(email, password, ip_address, origin)' do

    context 'password is correct' do

      it 'creates a session and returns token' do
        user = create :user, password: 'suraj123', email: 'suraj@pratap.com'
        expect(
            Session.fetch_token('suraj@pratap.com', 'suraj123', '127.0.0.1', 'http://firsttoface.com')
        ).to be_a(UUIDTools::UUID)
      end

      it 'deletes all the sessions from same ip_address and origin' do
        user = create :user, email: 'suraj@pratap.com', password: 'suraj123'
        create :session, origin: 'http://firsttoface.com', ip_address: '127.0.0.1', user: user
        expect{
            Session.fetch_token('suraj@pratap.com', 'suraj123', '127.0.0.1', 'http://firsttoface.com')
          #it will delete one record and create 1 more record,
          #so count changes by 0
        }.to change(Session, :count).by(0)
      end

    end

    context 'password is wrong' do

      it 'return nil' do

        create :user, email: 'suraj@pratap.com'

        expect(
            Session.fetch_token('suraj@pratap.com', 'some_password', 'ip_address', 'origin')
        ).to be_nil

      end

    end

    context 'email is invalid' do

      it 'raises an error' do

        create :user

        expect{
          Session.fetch_token('suraj@pratap.com', 'some_password', 'ip_address', 'origin')
        }.to raise_exception Session::SessionUserNotPresentException

      end

    end

    context 'Valid sessions with different origin and ip_addresses' do

      it 'is allows a user to create multiple sessions from different ip_addresses and origins' do

        user = create :user

        expect {

          user.sessions.new(ip_address: 'ip_1', origin: 'or_1').save!

          user.sessions.new(ip_address: 'ip_1', origin: 'or_2').save!

          user.sessions.new(ip_address: 'ip_2', origin: 'or_1').save!

          user.sessions.new(ip_address: 'ip_2', origin: 'or_2').save!

        }.to_not raise_exception

        expect{

          user.sessions.new(ip_address: 'ip_1', origin: 'or_1').save!

        }.to raise_exception

      end

    end

  end

end
