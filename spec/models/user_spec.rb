require 'rails_helper'

RSpec.describe User, type: :model do

  context 'Validations AND date consistency' do
    it 'is invalid without a first_name' do
      expect(build(:user, first_name: nil)).to_not be_valid
    end

    it 'is invalid without an email' do
      expect(build(:user, email: nil)).to_not be_valid
    end

    it 'is invalid with a duplicate email' do
      create(:user, email: 'suraj@pratap.com')
      expect(build(:user, email: 'suraj@pratap.com')).to_not be_valid
    end

    it 'is invalid with a invalid email' do
      expect(
          build(:user, email: 'surajpratap')
      ).to_not be_valid
    end

    #password is a attr for user which is not mapped to db
    it 'requires a password on create' do
      expect(
          build(:user, password: nil)
      ).to_not be_valid
    end

    it 'is invalid without a password_hash and password_salt' do

      #THIS IS IS TESTED PREMATURELY
      #ALL RECORDS WILL PASS THROUGH ENCRYPT_PASSWORD METHOD

      # user = build(:user, password_hash: nil)
      # expect(
      #
      # ).to_not be_valid
      # expect(
      #     build(:user, password_salt: nil)
      # ).to_not be_valid


      # WE CHECK THIS BY ASSIGNING PASSWORD=nil
      user = build :user, password: nil
      expect(user).to_not be_valid

    end

    it 'checks that user cannot implicitly specify password_hash and password_salt' do
      #THIS IS COVERED in
      #"checks that on update neither of password, password_hash or password_salt are present"
    end

    it 'checks that the length of password should be between >=6 to <=20' do
      expect(
          build :user, password: 'suraj'
      ).to_not be_valid
      expect(
          build :user, password: 'this is greater than 20 characters. should be invalid'
      ).to_not be_valid

      #BOUNDARY TEST
      expect(
          build :user, password: 'suraj1'
      ).to be_valid
      expect(
          build :user, password: '20characters1qwsdecf'
      ).to be_valid
      expect(
          build :user, password: '20characters1qwsdecf1'
      ).to be_valid
    end

    it 'checks that on update neither of password, password_hash or password_salt are present' do
      user = create :user
      user.password = 'some valid password'
      expect{
        user.save
      }.to raise_error
    end

    it 'validates that user records are never destroyed' do
      user = create :user
      user.destroy
      user = User.with_deleted.where(email: user.email).first
      expect(user).to_not be_nil
      expect(user).to be_a(User)
      expect(user.deleted_at).to be_a(Time)
    end

    it 'checks that column:id is a UUID' do
      user = create :user
      expect(user.id).to be_a(UUIDTools::UUID)
    end

    describe 'Username' do

      it 'is invalid without a username' do

        expect(
            build :user, username: nil
        ).to_not be_valid

      end

      it 'is invalid with a duplicate username' do

        user = create :user

        expect(
            build :user, username: user.username
        ).to_not be_valid

      end

      it 'is invalid if username contains non-word character' do

        invalid_user_names = [
            'su', #less than 3 chars
            'surajpratap12345n18i90', #more than 20 chars
            'suraj pratap', #conatains a space
            'suraj!' #non word character
        ]

        build(
            :user, username: invalid_user_names.sample
        )

      end

    end

  end

  context 'Instance methods' do

    describe 'User#name' do
      it 'returns the full name of user' do
        first_name = 'suraj'
        last_name = 'pratap'
        user = create :user, first_name: first_name, last_name: last_name
        expect(
            user.name
        ).to eq('suraj pratap')
      end
    end

    describe 'User#password_valid?' do
      #params 1. password
      #retuns true if user's password is correct
      #returns false if user's password is incorrect
      context 'password is correct' do
        it 'returns true' do
          password = 'suraj123'
          user = create :user, password: password
          expect(
              user.password_valid?(password)
          ).to be_truthy
        end
      end
      context 'password is incorrect' do
        it 'returns false' do
          user = create :user
          expect(
              user.password_valid?('some random string')
          ).to be_falsey
        end
      end
    end

    describe 'User#followers' do

      it 'returns all the followers of the user' do

        user = create :user

        Follower.create(user_id: user.id, follower_id: (create :user).id)

        Follower.create(user_id: user.id, follower_id: (create :user).id)

        expect(user.followers.count).to eq 2

      end

    end

    describe 'User#uplines' do

      it 'returns all the users that this user follows' do

        user = create :user

        Follower.create(follower_id: user.id, user_id: (create :user).id)

        Follower.create(follower_id: user.id, user_id: (create :user).id)

        expect(user.uplines.count).to eq 2

      end

    end

  end

  describe 'CALLBACK destroy followers after user-destroy' do

    it 'should destroy all follower record of the user after it is destroyed' do

      user = create :user

      Follower.create(user_id: user.id, follower_id: (create :user).id )

      Follower.create(user_id: user.id, follower_id: (create :user).id )

      expect{
        user.destroy
      }.to change(Follower, :count).by(-2)

    end

  end

end