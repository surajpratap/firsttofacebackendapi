require 'rails_helper'

RSpec.describe Quote, type: :model do

  describe 'Validations' do

    it 'is invalid without a user' do

      expect(
          build :quote, user: nil
      ).to_not be_valid

    end

    it 'is invalid without a statement' do

      expect(
          build :quote, statement: nil
      ).to_not be_valid

    end

    it 'should be deleted as the user is deleted' do

      user = create :user

      create :quote, user: user

      expect{
        user.destroy
      }.to change(Quote, :count).by(-1)

    end

  end

end
