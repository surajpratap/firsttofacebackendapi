require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  before(:each) do
    request.headers['HTTP_ORIGIN'] = 'http://firsttoface.com'
  end

  describe 'verify_email' do

    context 'user is not signed in' do

      it 'raises an error' do

        post :verify_email, nil, ControllerHelpers.headers

        expect(JSON.parse(response.body)).to eq({'errors' => ['Not signed in.']})

      end

      it 'returns an http response 406' do

        post :verify_email, nil, ControllerHelpers.headers

        expect(response.status).to eq 406

      end

    end

    context 'user is signed in' do

      before(:each) do
        @user = create :user
        @session = create :session, user: @user, ip_address: '0.0.0.0', origin: ControllerHelpers::ORIGIN
        request.headers['TOKEN'] = @session.id.to_param
      end

      it 'creates a email verification token for the signed in user' do

        post :verify_email, nil, ControllerHelpers.headers_with_token(@session)

        expect( assigns(:user).email_verification_token ).to_not be_nil

      end

      it 'sends an email to the user with the link to verify email'

      it 'returns an http response 200' do

        post :verify_email, nil, ControllerHelpers.headers_with_token(@session)

        expect(response.status).to eq 200

      end

    end

  end

end
