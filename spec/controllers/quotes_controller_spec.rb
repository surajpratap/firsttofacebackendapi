require 'rails_helper'

RSpec.describe QuotesController, type: :controller do

  describe 'Quotes#create' do

    #post /quotes

    context 'user is not signed in' do

      it 'does not create a new quote' do

        post :create, attributes_for(:quote), ControllerHelpers.headers

        expect(
            assigns(:quote)
        ).to be_nil

      end

    end

    context 'user is signed in' do

      before(:each) do

        @user = create :user

        @session = create :session, user: @user, ip_address: '0.0.0.0', origin: ControllerHelpers::ORIGIN

        request.headers['HTTP_TOKEN'] = @session.id.to_param

        request.headers['HTTP_ORIGIN'] = ControllerHelpers::ORIGIN

      end

      it 'creates a new quote' do

        expect{
          post :create, attributes_for(:quote)
        }.to change{Quote.count}.from(0).to(1)

      end

      it 'the new quote is associated with signed in user' do

        post :create, attributes_for(:quote)

        expect(
            assigns(:quote).user
        ).to eq(@user)

      end

    end

  end

  describe 'Quote#index' do

    #this method finds the last 10 quotes of the signed in user

    context 'user is not signed in' do

      it 'does not assigns quotes' do

        get :index

        expect( assigns(:quotes) ).to be_nil

      end

    end

    context 'user is signed in' do

      before(:each) do
        @user = create :user
        @session = create :session, user: @user, ip_address: '0.0.0.0', origin: ControllerHelpers::ORIGIN
        11.times do
          create :quote, user: @user
        end
        request.headers['HTTP_TOKEN'] = @session.id.to_param
        request.headers['HTTP_ORIGIN'] = ControllerHelpers::ORIGIN
      end

      it 'finds the last 10 quotes of user and assigns them' do
        get :index, nil, ControllerHelpers.headers_with_token(@session)
        expect(
            assigns(:quotes).length
        ).to eq 10
      end

    end

  end

end