require 'rails_helper'

RSpec.describe SessionsController, type: :controller do

  before(:each) do
    request.headers['HTTP_ORIGIN'] = 'http://firsttoface.com'
  end

  describe 'Sessions#create' do

    render_views

    context 'user is not signed in' do

      context 'valid params' do

        it 'should return a json, with token for accessing user' do

          create :user, password: 'suraj123', email: 'suraj@pratap.com'

          response = post :create, format: :json, email: 'suraj@pratap.com', password: 'suraj123'

          expect(JSON.parse(response.body)).to have_key('token')

        end

        it 'returns http 200' do

          create :user, password: 'suraj123', email: 'suraj@pratap.com'

          response = post :create, format: :json, email: 'suraj@pratap.com', password: 'suraj123'

          expect(response.status).to eq 200

        end

      end

      context 'invalid params' do

        it 'should return error' do

          response = post :create, format: :json, email: 'suraj@pratap.com', password: 'suraj123'

          expect(JSON.parse response.body).to have_key('errors')

        end

        it 'http code should be 406' do

          response = post :create, format: :json, email: 'suraj@pratap.com', password: 'suraj123'

          expect(response.status).to eq 406

        end

      end

    end

    context 'user is signed in' do

      before(:each) do

        user = create :user, email: '1@demo.com', password: 'suraj123'

        session = create :session, user: user, ip_address: '0.0.0.0', origin: 'http://firsttoface.com'

        request.headers['HTTP_TOKEN'] = session.id

      end

      it 'should return error' do

        response = post :create, format: :json, email: '1@demo.com', password: 'suraj123'

        expect(JSON.parse response.body).to have_key('errors')

      end

      it 'http code should be 406' do

        response = post :create, format: :json, email: '1@demo.com', password: 'suraj123'

        expect(response.status).to eq(406)

      end

    end

  end

  describe 'Sessions#destroy' do

    context 'user is signed in' do

      session_ = nil

      before(:each) do

        user = create :user

        session_ = create :session, user: user, ip_address: '0.0.0.0', origin: 'http://firsttoface.com'

        request.headers['HTTP_TOKEN'] = session_.id

      end

      it 'signs out the user and returns status 200' do

        response = delete :destroy, format: :json

        expect(response.status).to eq 200

        #Check that the user's session is actually destroyed
        expect(Session.where(id: session_.id).count).to eq 0

      end

    end

    context 'no one is signed in' do

      it 'returns an error and status 406' do

        response = delete :destroy, token: session.id

        expect(response.status).to eq 406

      end

    end

  end

end