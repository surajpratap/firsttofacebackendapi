FactoryGirl.define do
  factory :quote do
    statement{Faker::Lorem.sentence}
    association(:user)
  end

end
