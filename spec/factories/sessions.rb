
FactoryGirl.define do
  factory :session do
    ip_address{Faker::Internet.ip_v4_address}
    origin{Faker::Internet.url}
    association(:user)
  end

end
