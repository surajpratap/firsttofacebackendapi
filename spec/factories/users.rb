require 'faker'

FactoryGirl.define do

  factory :user do
    first_name{Faker::Name.first_name}
    last_name{Faker::Name.last_name}
    email{Faker::Internet.email}
    # password_hash Faker::Internet.password
    # password_salt Faker::Internet.password
    password{Faker::Internet.password(6, 20)}
    username{User::USERNAME_REGEXP.gen[0...20]}
    # cover_image{ File.open("#{Rails.root}/spec/cover_images/1.jpg") }
  end

end
