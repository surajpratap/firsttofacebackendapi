require 'rails_helper'

RSpec.describe 'GET /quotes', type: :request do

  context 'user is not signed in' do

    it 'returns an error' do

      get '/quotes', nil, ControllerHelpers.headers

      expect(json).to eq({'errors' => ['Not signed in.']})

    end

    it 'return http status 406' do

      get '/quotes', nil, ControllerHelpers.headers

      expect(status).to eq 406

    end

  end

  context 'user is signed in' do

    before(:each) do
      @user = create :user
      @session = create :session, user: @user, ip_address: ControllerHelpers::IP_ADDRESS, origin: ControllerHelpers::ORIGIN
      @quote1 = create :quote, user: @user
      @quote2 = create :quote, user: @user
    end

    it 'returns the last 10 quotes of the user' do

      get '/quotes', nil, ControllerHelpers.headers_with_token(@session)

      expect(json['quotes']).to match_array([
                                                {
                                                    'id' => @quote1.id.to_param,
                                                    'statement' => @quote1.statement,
                                                    'slug' => @quote1.slug
                                                },
                                                {
                                                    'id' => @quote2.id.to_param,
                                                    'statement' => @quote2.statement,
                                                    'slug' => @quote2.slug
                                                }
                                            ])

    end

    it 'returns http status 200' do

      get '/quotes', nil, ControllerHelpers.headers_with_token(@session)

      expect(status).to eq 200

    end

  end

end