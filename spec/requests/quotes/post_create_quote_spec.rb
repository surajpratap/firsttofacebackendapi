require 'rails_helper'

RSpec.describe 'CREATE SPEC', type: :request do

  context 'User is not signed in' do

    it 'returns an error' do
      post '/quotes', attributes_for(:quote), ControllerHelpers.headers
      expect(json).to eq({'errors' => ['Not signed in.']})
    end

    it 'returns http status 406' do
      post '/quotes', attributes_for(:quote), ControllerHelpers.headers
      expect(status).to eq 406
    end

  end

  context 'User is not signed in' do

    before(:each) do
      @user = create :user
      @session = create :session, user: @user, origin: ControllerHelpers::ORIGIN, ip_address: ControllerHelpers::IP_ADDRESS
    end

    it 'returns the newly created quote' do
      quote = attributes_for(:quote)
      post '/quotes', quote, ControllerHelpers.headers_with_token(@session)
      expect(json['quote'].keys).to match_array %w(id statement slug)
    end

    it 'returns http status 200' do
      post '/quotes', attributes_for(:quote), ControllerHelpers.headers_with_token(@session)
      expect(status).to eq 200
    end

  end

end