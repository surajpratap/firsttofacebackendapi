require 'rails_helper'

RSpec.describe 'GET QUESTIONS' do

  #this api returns the last 10 questions created

  #GET /questions

  context 'user is not signed in' do

    it 'returns an error' do

      get '/questions', nil, ControllerHelpers.headers

      expect(json).to eq({'errors' => ['Not signed in.']})

    end

    it 'returns http status 406' do

      get '/questions', nil, ControllerHelpers.headers

      expect(status).to eq 406

    end

  end

  context 'user is signed in' do

    before(:each) do

      @user = create :user

      @session = create :session, user: @user, ip_address: ControllerHelpers::IP_ADDRESS, origin: ControllerHelpers::ORIGIN

      @question1 = create :question, user: @user

      @question2 = create :question, user: @user

      get '/questions', nil, ControllerHelpers.headers_with_token(@session)

    end

    it 'returns the last 10 questions of current_user' do

      expect(json['questions']).to match_array([
                                          {
                                              'id' => @question2.id.to_param,
                                              'statement' => @question2.statement,
                                              'slug' => @question2.slug
                                          },
                                          {
                                              'id' => @question1.id.to_param,
                                              'statement' => @question1.statement,
                                              'slug' => @question1.slug
                                          }
                                      ])

    end

    it 'returns http status 200' do

      expect(status).to eq 200

    end

  end

end