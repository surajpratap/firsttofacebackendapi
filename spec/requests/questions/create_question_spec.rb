require 'rails_helper'

RSpec.describe 'CREATE QUESTION' do

  #This api creates a question for signed in user
  #reuires - sign-in
  #Endpoint = POST /questions

  context 'User is signed in' do

    before(:each) do

      @user = create :user

      @session = create :session, user: @user, ip_address: ControllerHelpers::IP_ADDRESS, origin: ControllerHelpers::ORIGIN

    end

    it 'created a record in questions table' do

      expect{
        post '/questions', {statement: Faker::Lorem.sentence(20)}, ControllerHelpers.headers_with_token(@session)
      }.to change(Question, :count).by(1)

    end

    it 'increases the number questions for user by 1' do

      expect(@user.questions.count).to eq 0

      post '/questions', {statement: Faker::Lorem.sentence(20)}, ControllerHelpers.headers_with_token(@session)

      expect(@user.questions.count).to eq 1

    end

    it 'returns the newly made question' do

      s = Faker::Lorem.sentence(20)

      post '/questions', {statement: s}, ControllerHelpers.headers_with_token(@session)

      expect(JSON.parse(response.body)['question']['statement']).to eq(s)

    end

    it 'returns http status 200' do

      s = Faker::Lorem.sentence(20)

      post '/questions', {statement: s}, ControllerHelpers.headers_with_token(@session)

      expect(response.status).to eq 200

    end

  end

  context 'User is not signed in' do

    before(:each) do
      post '/questions', {statement: Faker::Lorem.sentence}, ControllerHelpers.headers
    end

    it 'returns an error' do

      expect(JSON.parse response.body).to eq({'errors'=> ['Not signed in.']})

    end

    it 'returns http status 406' do

      expect(response.status).to eq 406

    end

  end

end