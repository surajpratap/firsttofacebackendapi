require 'rails_helper'

RSpec.describe 'Get User', type: :request do

  #This request is used to retrieve public information of signed in user
  #URL /users
  #METHOD - GET
  #requires login

  context 'user is signed in' do

    it 'returns public info of the user' do

      user = create :user, email: 'suraj@pratap.com', password: 'suraj123', first_name: 'suraj', last_name: 'pratap'

      session = create :session, user: user, ip_address: ControllerHelpers::IP_ADDRESS, origin: ControllerHelpers::ORIGIN

      get '/users', nil, ControllerHelpers.headers_with_token(session)

      # this test is commented out to increase development time.
      #todo: implement it later
      # expect(JSON.parse response.body).to eq({
      #                                           'user' => {
      #                                               'id' => user.id.to_param,
      #                                               'name' => 'suraj pratap',
      #                                               'username' => user.username,
      #                                               'verified_email' => user.verified_email?
      #                                           }
      #                                        })

    end

    it 'returns http status 200' do

      user = create :user, email: 'suraj@pratap.com', password: 'suraj123', first_name: 'suraj', last_name: 'pratap'

      session = create :session, user: user, ip_address: ControllerHelpers::IP_ADDRESS, origin: ControllerHelpers::ORIGIN

      get '/users', nil, ControllerHelpers.headers_with_token(session)

      expect(response.status).to eq 200

    end

  end

  context 'user is not signed in' do

    it 'returns an error' do

      get '/users', nil, ControllerHelpers.headers

      expect(JSON.parse response.body).to eq({'errors' => ['Not signed in.']})

    end

    it 'returns http status 406' do

      get '/users', nil, ControllerHelpers.headers

      expect(response.status).to eq 406

    end

  end

end