require 'rails_helper'

RSpec.describe "SignUps", type: :request do

  #signup is the process of creating a user
  # URL = POST /users

  context 'User is not signed in' do

    context 'valid params' do

      it 'creates a record in users table' do

        expect {
          post '/users', attributes_for(:user), ControllerHelpers.headers
        }.to change(User, :count).by 1

      end


      it 'returns status 200' do
        post '/users', attributes_for(:user), ControllerHelpers.headers
        expect(response.status).to eq 200
      end

    end

    context 'invalid params' do

      it 'does not change users table' do
        expect {
          post '/users', attributes_for(:user, username: 'sur pa'), ControllerHelpers.headers

        }.to change(User, :count).by 0
      end

      it 'returns status 422' do
        post '/users', attributes_for(:user, email: 'surajpratap.com'), ControllerHelpers.headers
        expect(response.status).to eq 422
      end

    end

  end

  context 'user is signed in' do

    it 'returns error' do

      user = create :user

      session_ = create :session, user: user, ip_address: ControllerHelpers::IP_ADDRESS, origin: ControllerHelpers::ORIGIN

      post '/users', attributes_for(:user), ControllerHelpers.headers_with_token(session_)

      expect(JSON.parse response.body).to eq({'errors' => ['Already signed in.']})

    end

    it 'returns status code 406' do

      user = create :user

      session_ = create :session, user: user, ip_address: ControllerHelpers::IP_ADDRESS, origin: ControllerHelpers::ORIGIN

      post '/users', attributes_for(:user), ControllerHelpers.headers_with_token(session_)

      expect(response.status).to eq 406

    end

  end

end
