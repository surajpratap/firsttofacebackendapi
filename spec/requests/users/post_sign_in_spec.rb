require 'rails_helper'

RSpec.describe "SignIns", type: :request do

  context 'user is signed in' do

    before(:each) do

      @user = create :user

      @session = create :session, user: @user, ip_address: '127.0.0.1', origin: 'http://firsttoface.com'

    end

    it 'should return error message: User is already signed in' do

      post '/sessions', {email: @user.email, password: @user.password}, {'HTTP_TOKEN' => @session.id, 'HTTP_ORIGIN' => 'http://firsttoface.com'}

      json = JSON.parse response.body

      expect(json).to eq({"errors" => ['Already signed in.']})

    end

    it 'returns status 406' do

      post '/sessions', {email: @user.email, password: @user.password}, {'HTTP_TOKEN' => @session.id, 'HTTP_ORIGIN' => 'http://firsttoface.com'}

      expect(response.status).to eq 406

    end

  end


  context 'user is not signed in' do

    context 'invalid params' do

      context 'email is wrong' do

        it 'returns error with code 406 and error message: user not present' do

          post '/sessions', {email: 'suraj@pratap@.com', password: 'suraj@123'}, ControllerHelpers.headers

          expect(JSON.parse response.body).to eq({ "errors" => ['No user found with this email/username.'] })

          expect(response.status).to eq 406

        end

      end

      context 'password is wrong' do

        it 'returns status 406 and message: password is wrong' do

          user = create :user, email: 'suraj@pratap.com', password: 'suraj123'

          post '/sessions', {email: user.email, password: 'wrong password'}, ControllerHelpers.headers

          expect(JSON.parse response.body).to eq({"errors" => ['password not correct.']})

        end

      end

    end

    context 'valid params' do

      before(:each) do

        @user = create :user, email: 'suraj@pratap.com', password: 'suraj123'

      end

      it 'creates a record in sessions table for user' do

        expect{
          post '/sessions', {email: 'suraj@pratap.com', password: 'suraj123'}, ControllerHelpers.headers
        }.to change(Session, :count).by(1)

      end

      it 'returns the id of newly created record as token' do

        post '/sessions', {email: 'suraj@pratap.com', password: 'suraj123'}, ControllerHelpers.headers

        last_created_session = Session.order('created_at desc').limit(1).first

        expect(JSON.parse response.body).to eq({'token' => last_created_session.id.to_param})

      end
      
      it 'returns status 200' do

        post '/sessions', {email: 'suraj@pratap.com', password: 'suraj123'}, ControllerHelpers.headers

        expect(response.status).to eq(200)

      end

    end

  end

end
