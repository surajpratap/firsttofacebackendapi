require 'rails_helper'

RSpec.describe "SignOuts", type: :request do

  #Signs the current user out
  #URL DELETE /sessions

  context 'user is signed in' do

    it 'destroys the session record of user' do

      user = create :user

      session = create :session, user: user, ip_address: '127.0.0.1', origin: 'http://firsttoface.com'

      expect(Session.count).to eq 1

      delete '/sessions', nil, ControllerHelpers.headers_with_token(session)

      expect(Session.count).to eq 0

    end

    it 'destroys all session records of user from this ip_address and origin' do

      user = create :user

      session1 = create :session, user: user, ip_address: '127.0.0.1', origin: 'http://firsttoface.com'

      expect(Session.count).to eq 1

      delete '/sessions', nil, ControllerHelpers.headers_with_token(session1)

      expect(Session.count).to eq(0)

    end

    it 'return status 200' do

      user = create :user

      session = create :session, user: user, ip_address: '127.0.0.1', origin: 'http://firsttoface.com'

      expect(Session.count).to eq 1

      delete '/sessions', nil, ControllerHelpers.headers_with_token(session)

      expect(Session.count).to eq 0

      expect(response.status).to eq 200

    end

  end

  context 'user is not signed in' do

    it 'returns error: not signed in' do

      delete '/sessions', nil, ControllerHelpers.headers

      expect(JSON.parse response.body).to eq({'errors' => ['Not signed in.']})

    end

    it 'returns status code 406' do

      delete '/sessions', nil, ControllerHelpers.headers

      expect(response.status).to eq 406

    end

  end


end
