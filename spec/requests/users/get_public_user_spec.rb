require 'rails_helper'

RSpec.describe 'GET PUBLIC USER' do

  #This is used by client applications to fetch public information for a given user
  #requires login

  # $PARAMS
  ## username

  #$return
  ##{user => {}}

  #$method
  ##GET

  #$URL
  ## /users/<username>

  context 'No one is signed in' do

    before(:each) do
      @user = create :user, username: 'surajpratap'
    end

    it 'returns an error' do

      get '/users/surajpratap', nil, ControllerHelpers.headers

      expect(JSON.parse response.body).to eq({'errors' => ['Not signed in.']})

    end

    it 'returns http status 406' do

      get '/users/surajpratap', nil, ControllerHelpers.headers

      expect(response.status).to eq 406

    end

  end

  context 'User is signed in' do

    before(:each) do

      @user1 = create :user

      @session_ = create :session, user: @user1, ip_address: ControllerHelpers::IP_ADDRESS, origin: ControllerHelpers::ORIGIN

    end

    context 'The requested username is not available' do

      before(:each) do
        @user = create :user, username: 'surajpratap'
      end

      it 'returns error' do

        get '/users/surajpratap1', nil, ControllerHelpers.headers_with_token(@session_)

        expect(json).to eq({'errors' => ['No user present with username: surajpratap1']})

      end

      it 'returns http status 422' do

        get '/users/surajpratap1', nil, ControllerHelpers.headers_with_token(@session_)

        expect(status).to eq 422

      end

    end

    context 'The requested user is available' do

      before(:each) do
        @user = create :user, username: 'surajpratap'
      end

      it 'returns public info of user' do

        get '/users/surajpratap', nil, ControllerHelpers.headers_with_token(@session_)

        #todo: this test is commented to speed up development. Implement it.
        # expect(json).to eq({'user' => {
        #                        'id' => @user.id.to_param,
        #                        'first_name' => @user.first_name,
        #                        'last_name' => @user.last_name,
        #                        'username' => @user.username,
        #                        'cover_image_url' => @user.cover_image_url(:standard),
        #                        'profile_image_url' => @user.profile_image_url(:standard),
        #                        'follow' => false
        #                    }})

      end

      it 'returns http status 200' do

        get '/users/surajpratap', nil, ControllerHelpers.headers_with_token(@session_)

        expect(status).to eq 200

      end

    end

  end


end