module RSpec
  module Rails
    module RequestExampleGroup
      def json
        @json ||= JSON.parse(response.body)
      end
    end
  end
end