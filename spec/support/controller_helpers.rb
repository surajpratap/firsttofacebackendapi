module ControllerHelpers

  ORIGIN = 'http://firsttoface.com'

  IP_ADDRESS = '127.0.0.1'

  def self.headers

    {'HTTP_ORIGIN' => 'http://firsttoface.com'}

  end

  def self.headers_with_token(session)
    {
        'HTTP_ORIGIN' => 'http://firsttoface.com',
        'HTTP_TOKEN' => session.id
    }
  end

end