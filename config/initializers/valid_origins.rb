#VALID_ORIGINS Array will contain all the white-listed origins.
#All other origins are rejected mercilessly.
VALID_ORIGINS = [
    'http://firsttoface.com',
    'https://firsttoface.com',
    'http://www.firsttoface.com',
    'https://www.firsttoface.com'
]