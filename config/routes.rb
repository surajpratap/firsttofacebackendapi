Rails.application.routes.draw do

  scope :sessions do
    post '/' => 'sessions#create'
    delete '/' => 'sessions#destroy'
  end

  scope :users do
    get '/' => 'users#info'
    post '/' => 'users#create'
    get ':username' => 'users#public_info'
    post 'cover_image' => 'users#cover_image'
    post 'profile_image' => 'users#profile_image'
    post 'verify_email' => 'users#verify_email'
    put '/' => 'users#update'
  end

  scope :questions do
    post '/' => 'questions#create'
    get '/' => 'questions#index'
    get ':slug' => 'questions#show'
  end

  scope :quotes do
    post '/' => 'quotes#create'
    get '/' => 'quotes#index'
    get ':slug' => 'quotes#show'
  end

  scope :feeds do
    get '/' => 'feeds#index'
  end

  scope :followers do
    post 'toggle' => 'followers#toggle'
  end

  scope :search do
    post 'search_by_username' => 'search#search_by_username'
  end

  scope :likes do
    post 'toggle' => 'likes#toggle'
  end

  scope :question_answers do
    post '/' => 'question_answers#create'
  end

  scope :quote_answers do
    post '/' => 'quote_answers#create'
  end

  scope :contacts do
    post '/' => 'contacts#create'
  end

  match '*path' => 'application#pass_options', via: 'options'

end
