# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150316151827) do

  create_table "contacts", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.text     "subject"
    t.text     "message"
    t.time     "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "followers", force: true do |t|
    t.uuid     "user_id"
    t.uuid     "follower_id"
    t.time     "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "likes", force: true do |t|
    t.string   "instance_type"
    t.uuid     "instance_id"
    t.uuid     "user_id"
    t.time     "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "question_answers", force: true do |t|
    t.text     "statement"
    t.uuid     "question_id"
    t.time     "deleted_at"
    t.uuid     "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "question_images", force: true do |t|
    t.uuid     "question_id"
    t.time     "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "questions", force: true do |t|
    t.uuid     "user_id"
    t.text     "statement"
    t.time     "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  create_table "quote_answers", force: true do |t|
    t.uuid     "quote_id"
    t.uuid     "user_id"
    t.text     "statement"
    t.time     "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "quote_images", force: true do |t|
    t.uuid     "quote_id"
    t.time     "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "quotes", force: true do |t|
    t.uuid     "user_id"
    t.time     "deleted_at"
    t.text     "statement"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  create_table "sessions", force: true do |t|
    t.uuid     "user_id"
    t.time     "deleted_at"
    t.string   "ip_address"
    t.string   "origin"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "last_request_at"
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.time     "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
    t.string   "cover_image_file_name"
    t.string   "cover_image_content_type"
    t.integer  "cover_image_file_size"
    t.datetime "cover_image_updated_at"
    t.string   "profile_image_file_name"
    t.string   "profile_image_content_type"
    t.integer  "profile_image_file_size"
    t.datetime "profile_image_updated_at"
    t.boolean  "verified_email",             default: false
    t.string   "email_verification_token"
    t.datetime "email_verification_sent_at"
    t.string   "gender"
    t.date     "date_of_birth"
  end

end
