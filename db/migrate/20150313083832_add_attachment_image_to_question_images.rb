class AddAttachmentImageToQuestionImages < ActiveRecord::Migration
  def self.up
    change_table :question_images do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :question_images, :image
  end
end
