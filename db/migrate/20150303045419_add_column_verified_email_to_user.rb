class AddColumnVerifiedEmailToUser < ActiveRecord::Migration
  def change
    add_column :users, :verified_email, :boolean, default: false
    add_column :users, :email_verification_token, :string
    add_column :users, :email_verification_sent_at, :datetime
  end
end
