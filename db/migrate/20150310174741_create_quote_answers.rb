class CreateQuoteAnswers < ActiveRecord::Migration
  def change
    create_table :quote_answers, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :quote_id
      t.uuid :user_id
      t.text :statement
      t.time :deleted_at

      t.timestamps
    end
  end
end
