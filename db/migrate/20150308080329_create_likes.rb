class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes, id: false do |t|
      t.uuid :id, primary_key: true
      t.string :instance_type
      t.uuid :instance_id
      t.uuid :user_id
      t.time :deleted_at

      t.timestamps
    end
  end
end
