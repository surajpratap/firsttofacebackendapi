class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :user_id
      t.time :deleted_at
      t.text :statement

      t.timestamps
    end
  end
end
