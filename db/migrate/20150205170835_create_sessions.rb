class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :user_id
      t.time :deleted_at
      t.string :ip_address
      t.string :origin

      t.timestamps
    end
  end
end
