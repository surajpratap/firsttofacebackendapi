class CreateQuestionImages < ActiveRecord::Migration
  def change
    create_table :question_images, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :question_id
      t.time :deleted_at

      t.timestamps
    end
  end
end
