class CreateQuoteImages < ActiveRecord::Migration
  def change
    create_table :quote_images, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :quote_id
      t.time :deleted_at

      t.timestamps
    end
  end
end
