class CreateQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :question_answers, id: false do |t|
      t.uuid :id, primary_key: true
      t.text :statement
      t.uuid :question_id
      t.time :deleted_at
      t.uuid :user_id

      t.timestamps
    end
  end
end
