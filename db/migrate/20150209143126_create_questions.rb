class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :user_id
      t.text :statement
      t.time :deleted_at

      t.timestamps
    end
  end
end
