class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts, id: false do |t|
      t.uuid :id, primary_key: true
      t.string :name
      t.string :email
      t.text :subject
      t.text :message
      t.time :deleted_at

      t.timestamps
    end
  end
end
