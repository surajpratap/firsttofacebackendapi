class CreateFollowers < ActiveRecord::Migration
  def change
    create_table :followers, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :user_id
      t.uuid :follower_id
      t.time :deleted_at

      t.timestamps
    end
  end
end
