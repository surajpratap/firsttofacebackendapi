class ContactsMailer < ActionMailer::Base
  default from: 'suraj.pratap24@gmail.com', to: 'suraj.pratap24@gmail.com'

  def new_contact(subject, email, message, name)

    @name = name

    @email = email

    @message = message

    mail(
        subject: subject
    )

  end

end
