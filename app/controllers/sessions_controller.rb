class SessionsController < ApplicationController

  before_action :authenticate_no_user, only: [:create]

  before_action :authenticate_user, only: [:destroy]

  #params 1.email, 2. password
  # return an access token for user if password is correct
  def create
    begin
      @token = Session.fetch_token(params[:email], params[:password], request.remote_ip, request.headers['HTTP_ORIGIN'])
      if @token.present?
        #render create.jbuilder
      else
        render status: :not_acceptable, json: {errors: ['password not correct.']}
      end
    rescue Session::SessionUserNotPresentException => e
      render status: :not_acceptable, json: {errors: [e.message]}.to_json
    end
  end

  def destroy
    session_ = current_user.sessions.where(ip_address: request.remote_ip, origin: request.headers['HTTP_ORIGIN'])
    if session_.any? and session_.destroy_all
      render status: :ok, nothing: true
    else
      render status: :unprocessable_entity, nothing: true
    end

  end

end