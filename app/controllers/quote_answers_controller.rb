class QuoteAnswersController < ApplicationController

  before_action :authenticate_user

  before_action :set_quote, only: [:create]

  #post /quotes_answers
  #params, :quote_id, :statement
  def create
    @quote_answer = @quote.quote_answers.new(
        statement: params[:statement],
        user_id: current_user.id
    )
    @quote_answer.save
    if @quote_answer.errors.any?
      render status: :unprocessable_entity
    else
      #render create.jbuilder
    end
  end

  private def set_quote
    @quote = Quote.find params[:quote_id]
  end

end