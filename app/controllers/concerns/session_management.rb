module SessionManagement

  def current_user
    #return previously calculated current_user in same request
    return @current_user if @current_user
    token = request.headers['HTTP_TOKEN']

    #a session is unique for a id, user, origin and ip_address
    session = Session.where(id: token, ip_address: request.remote_ip, origin: request.headers['HTTP_ORIGIN']).first

    if session.present?
      session.update_attribute :last_request_at, DateTime.now
      @current_user = session.user
    else
      nil
    end
  end

end