class LikesController < ApplicationController

  before_action :authenticate_user

  before_action :set_current_user

  #POST /likes/toggle
  #params :instance_type, :instance_id
  def toggle
    @like = Like.toggle(instance_type: params[:instance_type], instance_id: params[:instance_id], user_id: current_user.id)
    render status: :unprocessable_entity if @like.errors.any?
  end

  private def set_current_user
            @current_user = current_user
  end

end