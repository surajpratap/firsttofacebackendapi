class UsersController < ApplicationController

  before_action :authenticate_no_user, only: [:create]

  before_action :authenticate_user, only: [
                                      :info,
                                      :public_info,
                                      :cover_image,
                                      :profile_image,
                                      :verify_email
                                  ]

  def verify_email
    @user = current_user
    @user.generate_email_verification_token
    if @user.errors.any?
      render status: :unprocessable_entity, json: {errors: @user.errors.full_messages}
    else
      render status: :ok, nothing: true
    end
  end

  def profile_image
    @user = current_user
    @user.profile_image = params[:file]
    if @user.save
      #render profile_image.jbuilder
    else
      render status: :unprocessable_entity
    end
  end

  def cover_image
    @user = current_user
    @user.cover_image = params[:file]
    if @user.save
      #render cover_image.jbuilder
    else
      render status: :unprocessable_entity
    end
  end

  def info
    @user = current_user
  end

  def public_info
    #order is not important here since there should be only one user with one username
    #in other scenarios we should never use method :first
    #todo refactor it to pass public_info from User model
    @user = User.where(username: params[:username]).first
    @current_user = current_user
    @followers = @user.followers.order('created_at DESC').limit(10) if @user.present?
    @uplines = @user.uplines.order('created_at DESC').limit(10) if @user.present?
    render status: :unprocessable_entity unless @user.present?
  end

  def create
    @user = User.new(create_user_params)
    if @user.save
      #
    else
      render status: :unprocessable_entity
    end
  end

  def update
    @user = current_user
    params = update_user_params
    date = nil
    if params[:date].present? and params[:month].present? and params[:year].present?
      date = Date.new(params[:year], params[:month], params[:date])
    end
    if params[:gender].present?
      @user.gender = params[:gender]
    end
    if date.present?
      @user.date_of_birth = date
    end
    if @user.save
      render status: :ok, nothing: true
    else
      render status: :unprocessable_entity, nothing: true
    end
  end

  private def create_user_params
            params.permit(
                      :first_name,
                      :last_name,
                      :password,
                      :email,
                      :username
            )
  end

  private def update_user_params
            params.permit(
                      :gender,
                      :date,
                      :month,
                      :year
            )
  end

end