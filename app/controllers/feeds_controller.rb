class FeedsController < ApplicationController

  before_action :authenticate_user

  before_action :set_current_user

  def index
    @feed = current_user.feed
  end

  private def set_current_user
            @current_user = current_user
  end

end