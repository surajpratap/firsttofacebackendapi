class QuestionAnswersController < ApplicationController

  before_action :authenticate_user

  before_action :set_question, only: [:create]

  #post /question_answers
  #params, :question_id, :statement
  def create
    @question_answer = @question.question_answers.new(
                                                     statement: params[:statement],
                                                     user_id: current_user.id
    )
    @question_answer.save
    if @question_answer.errors.any?
      render status: :unprocessable_entity
    else
      #render create.jbuilder
    end
  end

  private def set_question
            @question = Question.find params[:question_id]
  end

end