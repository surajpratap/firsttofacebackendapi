class ApplicationController < ActionController::API

  #http authentication until launch
  # http_basic_authenticate_with name: 'suraj@firsttoface.com', password: '45rtfgvb^&'

  #this helps to use render jbuilder templates
  include ActionController::ImplicitRender

  before_action :validate_request

  before_action :set_headers

  include SessionManagement

  def authenticate_user
    render status: :not_acceptable, json: { errors: ['Not signed in.'] } unless current_user.present?
  end

  def authenticate_no_user
    render status: :not_acceptable, json: { errors: ['Already signed in.'] } if current_user.present?
  end

  #If the request is generated from an unauthenticated origin, it is stopped and a HTTP 406 is issued
  #frontend must look for 406 and do and apply for valid authentication

  #The system should render http 406 only for following cases
  #1. NON valid origin
  #2. Non presence of user auth token for user required tasks
  def validate_request
    # in development and test all requests are accepted
    return if Rails.env.development?
    #allow only origin from VALID_ORIGINS
    unless VALID_ORIGINS.include?(request.headers['HTTP_ORIGIN'])
      render json: {errors: ["The request is not generated from a valid origin.Request generated from #{request.headers['HTTP_ORIGIN']}, which is not registered with us."]}, status: :not_acceptable
    end
  end

  #set headers like access control origin, so browser does not rejects the response
  private def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, TOKEN'
  end

  #this method is supposed to handle all OPTIONS type requests
  #it is doing nothing
  def pass_options
    render status: :ok, nothing: true
  end

end
