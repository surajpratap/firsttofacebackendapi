class FollowersController < ApplicationController

  before_action :authenticate_user

  def toggle
    @follower = Follower.create_or_destroy(user_id: params[:user_id], follower_id: current_user.id)
    @current_user = current_user
    if @follower.errors.any?
      render status: :unprocessable_entity
    end
  end

end