class ContactsController < ApplicationController

  def create
    @contact = Contact.new(new_contact_params)
    if @contact.save
      render status: :ok, nothing: true
    else
      render status: :unprocessable_entity
    end
  end

  private def new_contact_params
            params.permit(
                      :name,
                      :subject,
                      :email,
                      :message
            )
  end

end