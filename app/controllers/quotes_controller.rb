class QuotesController < ApplicationController

  before_action :authenticate_user

  def index
    @quotes = current_user.quotes.order('created_at DESC').limit(10).includes(:quote_images)
  end

  def create
    @quote = current_user.quotes.new(create_quote_params)
    if @quote.save
      @quote.create_quote_images(params[:file]) if params[:file].present?
      #render create.jbuilder
    else
      render status: :unprocessable_entity
    end
  end

  def show
    @quote = Quote.where(slug: params[:slug]).first
    if @quote.present?
      #render show.jbuilder
    else
      render status: :not_found
    end
  end

  private def create_quote_params
            params.permit(
                      :statement
            )
  end

end