class QuestionsController < ApplicationController

  before_action :authenticate_user

  def index
    @questions = current_user.questions.order('created_at DESC').limit(10).includes(:question_images)
  end

  def create
    @question = current_user.questions.new(create_question_params)
    if @question.save
      @question.create_question_images(params[:file]) if params[:file].present?
      #render create.jbuilder
    else
      render status: :unprocessable_entity
    end
  end

  #get /questions/:slug
  #requires authentication
  def show
    @question = Question.where(slug: params[:slug]).first
    if @question.present?
      #render show.jbuilder with 200
    else
      render status: :not_found
    end
  end

  private def create_question_params
            params.permit(
                      :statement
            )
  end

end