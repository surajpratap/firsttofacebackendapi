class SearchController < ApplicationController

  before_action :authenticate_user

  before_action :strip_handle

  def search_by_username
    @users = User.search_by_username(params[:query])
  end

  private def strip_handle
            if params[:query].present? and params[:query][0] == '@'
              params[:query] = params[:query][1..-1]
            end
  end

end