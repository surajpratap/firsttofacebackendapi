class Question < ActiveRecord::Base

  include ActiveUUID::UUID

  # include LikeCount

  acts_as_paranoid

  belongs_to :user

  has_many :question_answers, dependent: :destroy

  has_many :question_images, dependent: :destroy

  validates :user, presence: true

  validates :statement, presence: true, length: {minimum: 20, maximum: 300}, format: /\s/ #should have at-least 1 space in it

  validate :slugify, on: :create

  validates :slug, presence: true

  private def slugify
            if self.statement.present?
              slug = self.statement.gsub(/\W/, '_')
              slug = slug.gsub(/__+/, '_')
              slug = slug[0..100]
              slug = "#{slug}_#{SecureRandom.uuid}" if self.class.where(slug: slug).any?
              self.slug = slug
            end
  end

  def create_question_images(images)
    images = images[0...5] #allow only first 5 images
    images.each do |image|
      question_image = self.question_images.new(image: image)
      question_image.save
    end
  end

end
