class Quote < ActiveRecord::Base

  include ActiveUUID::UUID

  # include LikeCount

  acts_as_paranoid

  belongs_to :user

  has_many :quote_answers, dependent: :destroy

  has_many :quote_images, dependent: :destroy

  validates :user, presence: true

  validates :statement, presence: true

  validate :slugify, on: :create

  validates :slug, presence: true

  private def slugify
    if self.statement.present?
      slug = self.statement.gsub(/\W/, '_')
      slug = slug.gsub(/__+/, '_')
      slug = slug[0..100]
      slug = "#{slug}_#{SecureRandom.uuid}" if self.class.where(slug: slug).any?
      self.slug = slug
    end
  end

  def create_quote_images(images)
    images = images[0...5]
    images.each do |image|
      quote_image = self.quote_images.new(image: image)
      quote_image.save
    end
  end

end
