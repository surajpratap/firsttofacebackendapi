class QuoteImage < ActiveRecord::Base

  include ActiveUUID::UUID

  include ContentManagement

  acts_as_paranoid

  belongs_to :quote

  has_attached_file :image, styles: {standard: '150x150#'}

  validates_attachment :image, :content_type => { :content_type => %w(image/jpeg image/png)}

end
