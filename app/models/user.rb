class User < ActiveRecord::Base

  USERNAME_REGEXP = /\A\w+\z/

  EMAIL_REGEXP = /\A[^@]+@[^@]+\z/

  include ActiveUUID::UUID

  include ContentManagement

  acts_as_paranoid

  attr_accessor :password

  has_many :sessions, dependent: :destroy

  has_many :questions, dependent: :destroy

  has_many :quotes, dependent: :destroy

  has_many :question_answers, dependent: :destroy

  has_many :quote_answers, dependent: :destroy

  validates :first_name, presence: true, length: { maximum: 20 }

  validates :last_name, length: {maximum: 20}

  validates :username, presence: true, uniqueness: true, format: { with: USERNAME_REGEXP }, length: { minimum: 3, maximum: 20 }

  validates :email, presence: true, uniqueness: true, format: { with: EMAIL_REGEXP }

  validates :password, presence: true, length: {minimum: 6, maximum: 50}, on: :create

  validate :encrypt_password, on: :create

  validates :password_hash, presence: true, on: :create

  validates :password_salt, presence: true, on: :create

  # validates :gender, inclusion: { in: %w(Male Female Other)  }, on: :update

  validate :cannot_update_password, on: :update

  validate :username_should_be_a_word

  has_attached_file :cover_image, styles: {standard: '1000'}, default_url: '/cover_images/:style/missing.png'

  validates_attachment :cover_image, :content_type => { :content_type => %w(image/jpeg image/png)}

  has_attached_file :profile_image, styles: {standard: '200x200#', thumb: '50x50#'}, default_url: '/profile_images/:style/missing.png'

  validates_attachment :profile_image, :content_type => { :content_type => %w(image/jpeg image/png)}

  #create FirstToFace user as the first follower and upline of this user

  after_create def create_first_follower
                 first_to_face_user = self.class.where(username: 'FirstToFace').first
                 if first_to_face_user
                   Follower.create(user_id: self.id, follower_id: first_to_face_user.id)
                   Follower.create(follower_id: self.id, user_id: first_to_face_user.id)
                 end
  end

  #######

  #ASSOCIATION METHOD
  #THIS WILL RETURN FOLLOWERS OF THE USER
  #TESTED IN USER_SPEC.RB
  def followers
    @followers ||= User.joins('LEFT OUTER JOIN followers ON users.id = followers.follower_id').where('followers.user_id = ?', self.id).where('followers.deleted_at IS NULL')
  end

  #THIS WILL RETURN ALL THE USER THAT ARE THIS USER FOLLOWS
  def uplines
    @uplines ||= User.joins('LEFT OUTER JOIN followers ON users.id = followers.user_id').where('followers.follower_id = ?', self.id).where('followers.deleted_at IS NULL')
  end

  #After destroying the user, all the follower records should be destroyed
  after_destroy def destroy_followers
                  Follower.where(user_id: self.id).destroy_all
  end

  #THIS IS A TEMPORARY METHOD TO RETURN THE FEED FOR USER
  #THIS WILL BE ENHANCED LATER
  #TODO: WRITE TEST FOR THIS
  def feed
    question = Question.where(user_id: self.uplines.map(&:id)).order('created_at DESC').limit(10).includes(:user)
    quotes = Quote.where(user_id: self.uplines.map(&:id)).order('created_at DESC').limit(10).includes(:user)
    feed = []
    question.each{ |q| feed << q }
    quotes.each{ |q| feed << q }
    feed.sort_by!{ |o| o.created_at }
    feed.reverse
  end

  private def username_should_be_a_word
    if self.username =~ /__+/
      self.errors.add(:username, 'cannot contain continuous underscores.')
    end
  end

  #THIS method tells whether this user follows the passed user
  def follows?(user)
    Follower.where(user_id: user.id, follower_id: self.id).any?
  end


  #This method generates a token for email verification and also adds the datetime at which it is generated at.
  def generate_email_verification_token
    self.update_attributes(
            verified_email: false,
            email_verification_token: SecureRandom.urlsafe_base64(36),
            email_verification_sent_at: DateTime.now
    )
  end

  def name
    "#{self.first_name} #{self.last_name}"
  end

  def password_valid?(password)
    #TODO WRITE SOME CODE TO COUNTER TIMER ATTACK
    self.password_hash == BCrypt::Engine.hash_secret(password, self.password_salt)
  end

  private def encrypt_password
    self.password_salt = BCrypt::Engine.generate_salt
    self.password_hash = BCrypt::Engine.hash_secret(self.password, self.password_salt)
  end

  private def cannot_update_password
            raise PasswordUpdateException, 'you cannot change password' if self.password.present? or self.password_hash_changed? or self.password_salt_changed?
  end

  #THIS IS CALLED AFTER RECORD IS CREATED
  #FOR SECURITY REASONS ATTR:PASSWORD IS SET TO nil
  after_create def destroy_password
                 self.password = nil
  end

  class PasswordUpdateException < Exception ;end

  ##############
  ###SEARCH#####

  def self.search_by_username(query, limit = 10)
    self.where('username LIKE ?', "#{query}%").limit(limit)
  end

  ##############

end