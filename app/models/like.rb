class Like < ActiveRecord::Base

  include ActiveUUID::UUID

  acts_as_paranoid

  validates :instance_type, presence: true

  validates :instance_id, presence: true

  validates :user_id, presence: true

  validate :user_has_not_already_liked

  validate :user_is_present

  validate :instance_is_present

  private def instance_is_present
            begin
              self.instance_type.constantize.find self.instance_id
            rescue Exception
              self.errors.add(:instance_type, 'is not present')
            end
  end

  private def user_is_present
            begin
              User.find self.user.id
            rescue Exception
              self.errors.add(:user_id, 'is not present.')
            end
  end

  private def user_has_not_already_liked
            if self.class.where(instance_type: self.instance_type, instance_id: self.instance_id, user_id: self.user_id).any?
              self.errors.add(:user_id, 'has already liked this.')
            end
  end

  def instance
    self.instance_type.constantize.find(self.instance_id)
  end

  def user
    User.find self.user_id
  end

  def self.toggle(instance_type:, instance_id:, user_id:)
    old_record = self.where(instance_type: instance_type, instance_id: instance_id, user_id: user_id).first
    if old_record.present?
      old_record.destroy
    else
      self.create(instance_type: instance_type, instance_id: instance_id, user_id: user_id)
    end
  end

end
