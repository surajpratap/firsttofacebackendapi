class Follower < ActiveRecord::Base

  include ActiveUUID::UUID

  acts_as_paranoid

  validates :user_id, presence: true

  validates :follower_id, presence: true

  validate :check_both_user_and_follower

  validate :check_for_redundant_record

  validate :check_user_and_follow_differ

  private def check_user_and_follow_differ
            if self.user == self.follower
              self.errors.add(:user_id, 'cannot follow themselves.')
            end
  end

  private def check_both_user_and_follower
            unless User.where('id = ?', self.user_id).any? or User.where('id = ?',  self.follower_id).any?
              self.errors.add(:user_id, 'is not present')
            end
  end

  private def check_for_redundant_record
            if self.class.where(user_id: self.user_id, follower_id: self.follower_id).any?
              self.errors.add(:follower_id, 'is already present')
            end
  end

  def self.create_or_destroy(user_id:, follower_id:)
    old_record = self.where(user_id: user_id, follower_id: follower_id).first
    if old_record.present?
      old_record.destroy
    else
      self.create(user_id: user_id, follower_id: follower_id)
    end
  end

  #returns the user who is being followed
  # @return [User]
  def user
    User.find self.user_id
  end

  #returns the user who is following
  # @return [User]
  def follower
    User.find self.follower_id
  end

end