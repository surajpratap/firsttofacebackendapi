class Contact < ActiveRecord::Base

  include ActiveUUID::UUID

  acts_as_paranoid

  validates :email, presence: true, format: { with: User::EMAIL_REGEXP }

  validates :subject, presence: true

  validates :message, presence: true

  after_create def send_email
                 ContactsMailer.new_contact(self.subject, self.email, self.message, self.name).deliver
  end

end
