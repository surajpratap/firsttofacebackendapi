module ContentManagement

  if Rails.env.development?
    PAPERCLIP_CONTENT_MANAGER = ''
  elsif Rails.env.test?
    PAPERCLIP_CONTENT_MANAGER = 'http://localhost:3000'
  else
    PAPERCLIP_CONTENT_MANAGER = ''
  end

# @param [Symbol] method
# @param [Array] args
  def method_missing(method, *args)
    split = method.to_s.split('_url')
    if split.length > 0 and self.respond_to?(split[0].to_sym)
      "#{PAPERCLIP_CONTENT_MANAGER}#{self.send(split[0].to_sym).url(args[0])}"
    else
      super
    end
  end

end