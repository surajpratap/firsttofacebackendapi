class QuoteAnswer < ActiveRecord::Base

  include ActiveUUID::UUID

  acts_as_paranoid

  belongs_to :quote

  belongs_to :user

  validates :statement, presence: true, length: {minimum: 20, maximum: 300}, format: /\s/ #should have at-least 1 space in it

  validates :user, presence: true

  validates :quote, presence: true

  #Although presence of user and question is checked
  #we check presence of user_id and question_id as well
  #Just two more extra validations for surety
  validates :user_id, presence: true

  validates :quote_id, presence: true
  #####

end
