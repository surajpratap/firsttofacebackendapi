class Session < ActiveRecord::Base

  include ActiveUUID::UUID

  acts_as_paranoid

  belongs_to :user

  validates :user_id, presence: true

  validates :user, presence: true

  validates :ip_address, presence: true

  validates :origin, presence: true

  validate :unique_attributes

  def unique_attributes
    if self.id.present?
      self.errors.add(:base, 'an active session already present') if Session.where(origin: self.origin, ip_address: self.ip_address, user_id: self.user_id).where('id != ?', self.id).any?
    else
      self.errors.add(:base, 'an active session already present') if Session.where(origin: self.origin, ip_address: self.ip_address, user_id: self.user_id).any?
    end
  end

  def self.fetch_token(email, password, ip_address, origin)
    user = User.where('email = ? OR username = ?', email, email).first
    raise SessionUserNotPresentException, 'No user found with this email/username.' unless user.present?
    if user.password_valid?(password)
      #delete all other sessions from this ip_address and origin
      user.sessions.where(ip_address: ip_address, origin: origin).destroy_all
      #create new session
      session = user.sessions.new(ip_address: ip_address, origin: origin)
      #save the new session
      #raise exception if new session is not created
      session.save!
      # return id of newly created session
      session.id
    else
      nil
    end
  end

  class SessionUserNotPresentException < Exception; end
end
