json.feed{
  json.array! @feed do |feed|
    json.type feed.class.to_s
    json.statement feed.statement
    json.slug feed.slug
    json.user{
      json.id feed.user.id
      json.username feed.user.username
      json.name feed.user.name
      json.profile_image feed.user.profile_image_url(:thumb)
    }
  end
}