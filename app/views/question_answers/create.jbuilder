if @question_answer.errors.any?
  json.errors @question_answer.errors.full_messages
else
  json.question_answer{
    json.id @question_answer.id
    json.statement @question_answer.statement
    json.user{
      json.id @question_answer.user.id
      json.name @question_answer.user.name
      json.username @question_answer.user.username
      json.profile_image @question_answer.user.profile_image_url(:thumb)
    }
  }
end