if @quote_answer.errors.any?
  json.errors @quote_answer.errors.full_messages
else
  json.quote_answer{
    json.id @quote_answer.id
    json.statement @quote_answer.statement
    json.user{
      json.id @quote_answer.user.id
      json.name @quote_answer.user.name
      json.username @quote_answer.user.username
      json.profile_image @quote_answer.user.profile_image_url(:thumb)
    }
  }
end