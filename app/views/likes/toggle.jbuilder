if Like.where(instance_type: params[:instance_type], instance_id: params[:instance_id], user_id: @current_user.id).any?
  json.like true
else
  json.like false
end