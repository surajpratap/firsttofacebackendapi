if @quote.errors.any?
  json.errors @quote.errors.full_messages
else
  json.quote{
    json.id @quote.id
    json.statement @quote.statement
    json.slug @quote.slug
    json.quote_images{
      json.array! @quote.quote_images do |quote_image|
        json.id quote_image.id
        json.url quote_image.image_url(:standard)
      end
    }
  }
end