if @quote.present?
  json.quote{
    json.id @quote.id
    json.statement @quote.statement
    json.user{
      json.id @quote.user.id
      json.name @quote.user.name
      json.username @quote.user.username
      json.profile_image @quote.user.profile_image_url(:thumb)
    }
    json.quote_answers{
      json.array! @quote.quote_answers.order('created_at DESC').limit(10).includes(:user) do |quote_answer|
        json.id quote_answer.id
        json.statement quote_answer.statement
        json.user{
          json.id quote_answer.user.id
          json.name quote_answer.user.name
          json.username quote_answer.user.username
          json.profile_image quote_answer.user.profile_image_url(:thumb)
        }
      end
    }
    json.quote_images{
      json.array! @quote.quote_images do |quote_image|
        json.id quote_image.id
        json.url quote_image.image_url(:standard)
      end
    }
  }
end