if @follower.errors.any?
  json.errors @follower.errors.full_messages
else
  if Follower.where(user_id: params[:user_id], follower_id: @current_user.id).any?
    json.follow true
  else
    json.follow false
  end
end