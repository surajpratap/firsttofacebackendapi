# noinspection RailsChecklist02
if @user.present? and @user.is_a? User
  json.user{
    # noinspection RailsChecklist02
    json.id @user.id
    json.first_name @user.first_name
    json.last_name @user.last_name
    json.username @user.username
    json.cover_image_url @user.cover_image_url(:standard)
    json.profile_image_url @user.profile_image_url(:standard)
    # noinspection RailsChecklist02
    json.follow @current_user.follows?(@user)
    json.number_of_followers @user.followers.count
    json.number_of_uplines @user.uplines.count
    json.followers{
      # noinspection RailsChecklist02
      json.array! @followers do |follower|
        json.id follower.id
        json.name follower.name
        json.username follower.username
        json.profile_image follower.profile_image_url(:thumb)
      end
    }
    json.uplines{
      # noinspection RailsChecklist02
      json.array! @uplines do |upline|
        json.id upline.id
        json.name upline.name
        json.username upline.username
        json.profile_image upline.profile_image_url(:thumb)
      end
    }
  }
else
  json.errors ["No user present with username: #{params[:username]}"]
end