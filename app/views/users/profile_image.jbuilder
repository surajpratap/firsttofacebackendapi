if @user.errors.any?
  json.errors @user.errors.full_messages
else
  json.user{
    json.profile_image_url @user.profile_image_url(:standard)
  }
end