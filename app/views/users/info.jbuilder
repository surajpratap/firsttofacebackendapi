json.user{
  json.id @user.id
  json.name @user.name
  json.username @user.username
  json.verified_email @user.verified_email?
  json.profile_image @user.profile_image_url(:thumb)
}