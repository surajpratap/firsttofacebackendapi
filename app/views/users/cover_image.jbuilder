if @user.errors.any?
  json.errors @user.errors.full_messages
else
  json.user{
    json.cover_image_url @user.cover_image_url(:standard)
  }
end