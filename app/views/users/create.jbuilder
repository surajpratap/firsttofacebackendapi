if @user.errors.any?
  json.errors @user.errors.full_messages
else
  json.user{
    json.first_name @user.first_name
    json.last_name @user.last_name
    json.email @user.email
  }
end