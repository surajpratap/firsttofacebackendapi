if @question.present?
  json.question{
    json.id @question.id
    json.statement @question.statement
    json.user{
      json.id @question.user.id
      json.name @question.user.name
      json.username @question.user.username
      json.profile_image @question.user.profile_image_url(:thumb)
    }
    json.question_answers{
      json.array! @question.question_answers.order('created_at DESC').limit(10).includes(:user) do |question_answer|
        json.id question_answer.id
        json.statement question_answer.statement
        json.user{
          json.id question_answer.user.id
          json.name question_answer.user.name
          json.username question_answer.user.username
          json.profile_image question_answer.user.profile_image_url(:thumb)
        }
      end
    }
    json.question_images{
      json.array! @question.question_images do |question_image|
        json.id question_image.id
        json.url question_image.image_url(:standard)
      end
    }
  }
end