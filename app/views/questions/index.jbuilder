json.questions{
  json.array! @questions do |question|
    json.id question.id
    json.statement question.statement
    json.slug question.slug
    json.question_images{
      json.array! question.question_images do |question_image|
        json.id question_image.id
        json.url question_image.image_url(:standard)
      end
    }
  end
}